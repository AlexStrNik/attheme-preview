const sharp = require(`sharp`);
const sizeOf = require(`image-size`);

module.exports = function dataWallpaperHandler (document, theme, Params) {
  if (!theme.chat_wallpaper) {

    const elements = Array.from(document.querySelector('[data-wallpaper]'));

    const imgs = elements.map(async (element) => {
      let IMAGE_WIDTH  = Number(element.getAttribute('width'));
      let IMAGE_HEIGHT = Number(element.getAttribute('height'));

      let CONTAINER_RATIO = IMAGE_HEIGHT / IMAGE_WIDTH;

      if (this.getVariable(Attheme.IMAGE_KEY, theme, Params) && !theme.chat_wallpaper) {
        const imageBuffer = Buffer.from(await this.getVariable(Attheme.IMAGE_KEY, theme, Params), `binary`);

        const { width, height } = sizeOf(imageBuffer);
        const imageRatio = height / width;

        let finalHeight;
        let finalWidth;

        if (CONTAINER_RATIO > imageRatio) {
          finalHeight = IMAGE_HEIGHT;
          finalWidth = Math.round(IMAGE_HEIGHT / imageRatio);
        } else {
          finalWidth = IMAGE_WIDTH;
          finalHeight = Math.round( IMAGE_WIDTH * imageRatio);
        }

        const resizedImage = await sharp(imageBuffer)
          .resize(finalWidth, finalHeight)
          .png()
          .toBuffer();

        const croppedImage = await sharp(resizedImage)
          .resize(IMAGE_WIDTH, IMAGE_HEIGHT)
          .crop()
          .png()
          .toBuffer();

        element.setAttribute(`xlink:href`,`data:image/png;base64,${croppedImage.toString(`base64`)}`);

      }
    });

    return Promise.all(imgs);
  }
};
