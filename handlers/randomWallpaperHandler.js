const path = require(`path`);
const Attheme = require(`attheme-js`);
const fs = require(`fs`);

module.exports = function randomWallpaperHandler (theme, Params) {
  const WALLPAPERS_AMOUNT = 32;

  if (theme[Attheme.IMAGE_KEY] || theme.chat_wallpaper) {
    return theme[Attheme.IMAGE_KEY];
  }

  const randomWallpaper = Math.floor(Math.random() * WALLPAPERS_AMOUNT);
  const image = fs.readFileSync(
    path.join(__dirname, `wallpapers/${randomWallpaper}.jpg`),
    `binary`,
  );

  return image;
};
