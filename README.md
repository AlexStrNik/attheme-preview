# .attheme-preview 

A JavaScript package for generating previews of .attheme's, created by [@AlexStrNik] & [@SnejUgal].

[@AlexStrNik]: https://t.me/AlexStrNik
[@SnejUgal]: https://t.me/SnejUgal

## Installing
```bash
$npm i attheme-preview
```

## Example β
```js
import Attheme from "attheme-js";
import Preview from "attheme-preview";
import {
  randomWallpaperHandler,
  chatWallpaperHandler,
} from "attheme-preview/handlers";
import defaultTemplate from "attheme-preview/default-template";
import { writeFile } from "fs";

const theme = new Attheme(`
windowBackgroundWhite=-328966
`);

const preview = new Preview();

preview.setVariableHandler(Attheme.IMAGE_KEY, randomWallpaperHandler);
preview.addCustomHandler(chatWallpaperHandler);

preview.createPreview(theme, defaultTemplate)
  .then(async (previewBuffer) => {
      await fs.writeFile(`./preview.png`, previewBuffer);
      console.log(`Done!`);
  });
```

## Fonts α

> Not implented yet..

## Params β

### `Params.sharp.density: number = 150`

Density the preview should be rendered with.

### `Params.sharp.extension: "jpeg" | "png" | "webp" | "tiff" = "png"`

The image type the rendered preview should be of.

### `Params.sharp.imageOptions: WebpOptions | JpegOptions | PngOptions | TiffOptions = {}`

The rendering params that are passed to `sharp` directly.


## JS / TS API β

```ts
import { JSDOM } from "jsdom";
import { JpegOptions, PngOptions, TiffOptions, WebpOptions } from "sharp";

const IMAGE_KEY = Symbol.for(`image`);

type RawTheme = Theme | Buffer | string;

type Template = JSDOM | string;

interface SharpParams {
  density?: number;
  extension?: "jpeg" | "png" | "webp" | "tiff";
  imageOptions?: WebpOptions | JpegOptions | PngOptions | TiffOptions;
}

interface Params {
  sharp?: SharpParams;
}

interface Color {
  red: number;
  green: number;
  blue: number;
  alpha: number;
}

interface Theme {
  [key: string]: Color;
  [IMAGE_KEY]?: string;
}

interface VariableHandler {
  (theme: Theme, params: Params): Color;
}

interface ClassHandler {
  (element: SVGElement, theme: Theme, params: Params): void;
}

interface CustomHandler {
  (preview: JSDOM, theme: Theme, params: Params): void;
}
```

### `new Preview()`

Creates a new instance of `Preview`.

### `preview.setVariableHandler(variable: string | IMAGE_KEY, handler: VariableHandler)`

Registers a new getter for `variable`. It isn't a usual property getter; it receives the theme and the passed params from `preview.createPreview`.

### `preview.setClassHandler(className: string, handler: ClassHandler)`

Registers a new handler for elements with `className` class. After it's dome with coloring the template, `attheme-preview` calls registered handlers for each element of the `className` class. For example, if you register a handler for `divider`, and there are six elements with `class="divider"`, then the handler is called six times, receiving another `class="divider"` element each time.

It is run after `attheme-preview` filled the preview template with colors, but before custom ones.

### `preview.addCustomHandler(handler: CunstomHandler)`

Registers a new handler that's run after the class ones. It receives the whole colored template tree. 

### `preview.createPreview(theme: RawTheme, template: Template, params?: Params): Promise<Buffer>`

Generates a new preview, and returns a buffer of a .png image (by Default).

The `params` argument isn't used by this method, but it is passed to the handlers.

## XML API α

### `<path class="className"/>` or `rect` or `circle`

Sets the `fill` attribute to `preview.getVariable(className, ...)`.

### `<image class="IMG" width="width" height="height"/>` @deprecated

Sets the `xlink:href` attribute to `preview.getVariable(Attheme.IMAGE_KEY, ...)`.

The handler ``require(`attheme-preview/handlers/chatWallpaperHandler.js`)`` must be added to make it work.

## TODO

### Remove deprecated code

e.g: `xlink:href`

### Add custom fonts support

check `puggo:cairo` bindings

### Add more default Handlers

e.g: `gradient` support, `text` support

### RORO (“Receive object, return object” pattern)

implent `RORO` everewhere

