const Attheme = require(`attheme-js`);
const defaultVariablesValues = require(`attheme-default-values`).default;
const { JSDOM } = require(`jsdom`);
const sharp = require(`sharp`);
const Color = require(`@snejugal/color`).default;
const defaultSharpParams = {
  density: 150,
  extension: `png`,
};

const readXml = (template) => {
  if (template.constructor.name == "DocumentFragment") {
    return template.firstChild;
  }
  return JSDOM.fragment(template).firstChild;
};

class Preview {

  constructor(){
    this.classHandlers = {}; // {className: fn(element, theme, Params)}
    this.variableHandlers = {}; // {variableName: fn(theme, Params)}
    this.customHandlers = []; // [fn(preview, theme, Params)]
  }

  fill(element, color){
    element.setAttribute(`fill`, Color.createCssRgb(color));
  }

  async getVariable(variableName, theme, Params){
    if (this.variableHandlers[variableName]) {
      return this.variableHandlers[variableName](theme, Params);
    }

    return theme[variableName] || defaultVariablesValues[variableName];
  }

  setVariableHandler(variableName, fn){
    this.variableHandlers[variableName] = fn.bind(this);
  }

  addCustomHandler(fn){
    this.customHandlers.push(fn.bind(this));
  }

  setClassHandler(className, fn) {
    this.classHandlers[className] = fn.bind(this);
  }

  async createPreview({
    theme: Theme,
    template: Template,
    params: Params = {},
  }) {
    let theme;

    if (Theme instanceof Buffer){
      theme = new Attheme(Theme.toString(`binary`));
    } else if (typeof Theme === `string`) {
      theme = new Attheme(Theme);
    } else {
      theme = Theme;
    }
    const preview = await readXml(Template);

    for (const variable in defaultVariablesValues) {
      if (variable === `chat_wallpaper` && !theme.chat_wallpaper) {
        continue;
      }

      const elements = Array.from(preview.getElementsByClassName(variable));
      const color = await this.getVariable(variable, theme, Params);

      await Promise.all(elements.map((element) => {
        return this.fill(element,color)
      }));
    }

    await Promise.all(Object.keys(this.classHandlers).map((Class)=>{
      return Promise.all(preview.getElementsByClassName(Class).forEach((element) => {
        return this.classHandlers[Class](element, theme, Params);
      }));
    }));

    await Promise.all(this.customHandlers.map((fn) => {
      return fn(preview, theme, Params);
    }));

    const previewBuffer = Buffer.from(preview.outerHTML);

    const { density, extension, imageOptions } = Params && Params.sharp || defaultSharpParams;

    return await sharp(previewBuffer, {
      density,
    })[extension](imageOptions).toBuffer();
  };
}

module.exports = Preview;
